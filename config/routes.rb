Rails.application.routes.draw do
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    # https://...amazonaws.com/index入力してenterを押すとtop_controllerの中にあるactionを実行する
    # get '/index', to: 'top#index'
    # E https://...amazonaws.com と入力してenterを押すとtop_controllerの中にあるactionを実行するように変更
    root :to => 'top#index'
    get '/reward', to:'top#reward'
    get '/service', to:'top#service'
    get '/approach', to:'top#approach'
    get '/company', to:'top#company'
    get '/contact', to:'top#contact'
    get '/index', to:'inheritance#index'
    get '/representative', to:'top#representative'
    get '/consultation', to:'consultation#index'
end
